<?php

require_once ('animal.php');
require_once ('Frog.php');
require_once ('Ape.php');

$sheep = new Animal("Shaun");
echo "Nama hewan : " . $sheep -> name . "<br>";
echo "Jumlah kaki : " . $sheep -> legs . "<br>";
echo "Cold blooded : " . $sheep -> cold_blooded . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Nama hewan : " . $sungokong -> name . "<br>";
echo "Jumlah kaki : " . $sungokong -> legs . "<br>";
echo "Cold blooded : " . $sungokong -> cold_blooded . "<br>";
echo $sungokong -> yell() . "<br> <br>";

$kodok = new Frog("buduk");
echo "Nama hewan : " . $kodok -> name . "<br>";
echo "Jumlah kaki : " . $kodok -> legs . "<br>";
echo "Cold blooded : " . $kodok -> cold_blooded . "<br>";
echo $kodok -> jump() . "<br>";

?>