
@extends('layout.master')

@section('title')
    Buat Accout Baru!
@endsection

@section('content')
    
<form action="/welcome" method="post">
    @csrf
    <h3>Sign Up Form</h3>

    <label>First Name:</label><br><br>
        <input type="text" name="fname"><br><br>

    <label>Last Name:</label><br><br>
        <input type="text" name="lname"><br><br>

    <label>Gender:</label><br><br>
        <input type="radio" name="jk" value="male">Male <br>
        <input type="radio" name="jk" value="female">Female <br>
        <input type="radio" name="jk" value="other">Other <br>
    <br>

    <label>Nationality:</label><br><br>
        <select name="kebangsaan">
            <option value="ID">Indonesian</option>
            <option value="SG">Singaporean</option>
            <option value="MY">Malaysian</option>
            <option value="AU">Australian</option>
        </select> <br>
    <br>

    <label>Language Spoken:</label><br><br>
        <input type="checkbox" value="ID">Bahasa Indonesia <br>
        <input type="checkbox" value="ENG">English <br>
        <input type="checkbox" value="Oth">Other <br> 
    <br>

    <label>Bio:</label><br><br>
    <textarea name="biodata" cols="30" rows="10"></textarea>
    <br>

    <input type="submit" value="Sign Up">
</form>
@endsection
