@extends('layout.master')

@section('title')
    Halaman Tambah Pemeran
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Pemeran</label>
      <input name="nama" type="text" value="{{old('nama')}}" class="form-control">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Pemeran</label>
      <input name="umur" type="number" value="{{old('umur')}}" class="form-control">
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection