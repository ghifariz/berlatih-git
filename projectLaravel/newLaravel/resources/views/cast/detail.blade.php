@extends('layout.master')

@section('title')
    Halaman Detail Pemeran
@endsection

@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<h3>Umur : {{$cast->umur}} Tahun</h3>

<a href="/cast"class="btn btn-secondary my-2">Kembali</a>

@endsection