<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Home']);
Route::get('/register', [AuthController::class, 'Register']);
Route::post('/welcome', [AuthController::class, 'Welcome']);

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});

// CRUD cast
// Create Data (Route untuk mengarah ke from tambah cast)
Route::get('/cast/create', [CastController::class , 'create']);
// Route untuk menyimpan data inputan ke database
Route::post('/cast', [CastController::class , 'store']);

// Read Data (menampilkan semua data dari database)
Route::get('/cast', [CastController::class , 'index']);
// Detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class , 'show']);

// Update Data (Route untuk mengarah ke form update berdasarkan id)
Route::get('/cast/{cast_id}/edit', [CastController::class , 'edit']);
// Update Data berdasarkan id di database
Route::put('/cast{cast_id}', [CastController::class , 'update']);

// Delete Data
Route::delete('/cast/{cast_id}', [CastController::class , 'destroy']);
