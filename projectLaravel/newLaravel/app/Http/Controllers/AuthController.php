<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register()
    {
        return view('register');
    }

    public function Welcome(Request $request)
    {
        $fullName = $request['fname'] . " " . $request['lname'];

        return view('welcome', ['fullName' => $fullName]);
    }
}
