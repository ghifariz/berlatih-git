<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\FuncCall;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required|min:2',
        ],
        [
            'nama.required' => "Nama harus diisi. Tidak boleh kosong",
            'umur.required' => "Umur harus diisi. Tidak boleh kosong",
            'nama.min' => "Nama harus minimal 2 karakter",
            'umur.min' => "Umur harus minimal 2 karakter"
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur']
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.view', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', ['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.edit', ['cast'=>$cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'umur' => 'required|min:2',
        ],
        [
            'nama.required' => "Nama harus diisi. Tidak boleh kosong",
            'umur.required' => "Umur harus diisi. Tidak boleh kosong",
            'nama.min' => "Nama harus minimal 2 karakter",
            'umur.min' => "Umur harus minimal 2 karakter"
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                ]
            );

            return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
